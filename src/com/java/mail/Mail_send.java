package com.java.mail;

import java.io.IOException;
import java.sql.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.ParseConversionEvent;

public class Mail_send {
	
	LoadPropeties setPropieties = new LoadPropeties();
	MailReader reader = new MailReader();
	public String asunto ;
	public String mensaje;
	
	public Mail_send() throws IOException{

	
		String Subject = reader.getSubject();
		Read_Excel data = new Read_Excel(Subject);
		data.setInputFile(setPropieties.getExcelpath());
		data.read();
		String source = reader.getSource()[0].toString();
		String mailTo = data.getMailTo();
		int sourcesendin = source.indexOf('<');
		int sourcesendout = source.indexOf('>');
		String sourcefinal = source.substring(sourcesendin+1,sourcesendout);		 
		SendMessageConfirm(Subject,sourcefinal,data.NITexist);
		
		if (data.NITexist==true){
			SendMessage(Subject,mailTo);
		}else{SenMessageNITnoExist(Subject);}
	}
	public void SendMessageConfirm(String Subject,String sourcefinal,boolean notify){

		String servidorSMTP = setPropieties.getSMTPserver();
		String puerto = setPropieties.getPort();
		String usuario = setPropieties.getMailuser();
		String password = setPropieties.getPasswduser();
		String destino = sourcefinal;
		if(notify == true){
			asunto = setPropieties.getSubjectmessageconfirm();
			mensaje = setPropieties.getBodymessageconfirm();
		}else {
			asunto = setPropieties.getSubjectNITnotfoundsource();	
			mensaje = setPropieties.getBodymessageNITnotfoundsource();
		}
		Properties props = new Properties();
		props.put("mail.debug", "true");
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.host", servidorSMTP);
		props.put("mail.smtp.port", puerto);

		Session session = Session.getInstance(props, null);

		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					destino));
			message.setSubject(asunto);
			message.setText(mensaje);

			Transport tr = session.getTransport("smtp");
			tr.connect(servidorSMTP, usuario, password);
			message.saveChanges();   
			tr.sendMessage(message, message.getAllRecipients());
			tr.close();

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	public void SenMessageNITnoExist (String Subject){

		String servidorSMTP = setPropieties.getSMTPserver();
		String puerto = setPropieties.getPort();
		String usuario = setPropieties.getMailuser();
		String password = setPropieties.getPasswduser();

		String destino = setPropieties.MailtosendNITnotfound;
		String asunto = setPropieties.SubjectNITnotfound;
		String mensaje = setPropieties.BodymessageNITnotfound;
		Properties props = new Properties();
		props.put("mail.debug", "true");
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.host", servidorSMTP);
		props.put("mail.smtp.port", puerto);
		Session session = Session.getInstance(props, null);
		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					destino));
			message.setSubject(asunto);
			message.setText(mensaje);

			Transport tr = session.getTransport("smtp");
			tr.connect(servidorSMTP, usuario, password);
			message.saveChanges();   
			tr.sendMessage(message, message.getAllRecipients());
			tr.close();

		} catch (MessagingException e) {
			e.printStackTrace();
		}



	}
	public void SendMessage(String Subject,String mailTo){

		String servidorSMTP = setPropieties.getSMTPserver();
		String puerto = setPropieties.getPort();
		String usuario = setPropieties.getMailuser();
		String password = setPropieties.getPasswduser();

		String destino = mailTo;
		String asunto = setPropieties.getSubjectmessage();
		String mensaje = setPropieties.getBodymessage();

		Properties props = new Properties();

		props.put("mail.debug", "true");
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.host", servidorSMTP);
		props.put("mail.smtp.port", puerto);

		Session session = Session.getInstance(props, null);

		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					destino));
			message.setSubject(asunto);
			message.setText(mensaje);
			Transport tr = session.getTransport("smtp");
			tr.connect(servidorSMTP, usuario, password);
			message.saveChanges();   
			tr.sendMessage(message, message.getAllRecipients());
			tr.close();

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
}

