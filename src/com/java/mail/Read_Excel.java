package com.java.mail;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Read_Excel {

	private String inputFile;
	public String Subjectx;
	public String mailTo;
	boolean NITexist = false;
	public String mailNit;
	
	public void read() throws IOException  {
		File inputWorkbook = new File(inputFile);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(0);
			for (int j = 0; j < sheet.getColumns(); j++) {
				for (int i = 0; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					CellType type = cell.getType();

					if (type == CellType.NUMBER) {
						String celda = cell.getContents(); 
						Cell cellmail = sheet.getCell((j+1),i); 
						mailTo = cellmail.getContents();
						compareNIT(Subjectx, celda);
						if(NITexist==true)
						{
							break;
						}
					}
				}
			}
		} catch (BiffException e) {
			e.printStackTrace();
		}
	}
	public boolean compareNIT(String subject,String cel){

		if (Subjectx.equalsIgnoreCase(cel)){
			NITexist = true;	   

		}else{NITexist = false;
		}
		return NITexist;
	}
	public String getMailTo() {
		return mailTo;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	public void setNITexist(boolean nITexist) {
		NITexist = nITexist;
	}
	public Read_Excel(String subjectsend){
		this.Subjectx=subjectsend;		
	}
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}
	public boolean isNITexist() {
		return NITexist;
	}
} 

