package com.java.mail;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

public class MailReader
{

	public  Message messages[];
	Folder inbox;
	String subject;
	Address[] target;
	Address[] source;
	LoadPropeties setReaderPropeties = new LoadPropeties();

	public MailReader() throws InvalidPropertiesFormatException, IOException
	{

		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		try
		{
			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com",setReaderPropeties.getMailuserimap(), setReaderPropeties.getPasswduserimap());
			inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_ONLY);
			Message messages[] = inbox.search(new FlagTerm(new Flags(Flag.SEEN), false));
			FetchProfile fp = new FetchProfile();
			fp.add(FetchProfile.Item.ENVELOPE);
			fp.add(FetchProfile.Item.CONTENT_INFO);
			inbox.fetch(messages, fp);
			try
			{
				printAllMessages(messages);
				inbox.close(true);
				store.close();
			}
			catch (Exception ex)
			{
				System.out.println("Exception arise at the time of read mail");
				ex.printStackTrace();
			}
		}
		catch (NoSuchProviderException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
			System.exit(2);
		}
	}
	public void printAllMessages(Message[] msgs) throws Exception
	{
		for (int i = 0; i < msgs.length; i++)
		{
			printEnvelope(msgs[i]);
		}
	}
	public void printEnvelope(Message message) throws Exception
	{
		// FROM
		if ((source = message.getFrom()) != null)
		{
			for (int j = 0; j < source.length; j++)
			{
				System.out.println("FROM: " + source[j].toString());
			}
		}
		// TO
		if ((target = message.getRecipients(Message.RecipientType.TO)) != null)
		{
			for (int j = 0; j < target.length; j++)
			{
				System.out.println("TO: " + target[j].toString());
			}
		}
		subject = message.getSubject();
		Date receivedDate = message.getReceivedDate();
		String content = message.getContent().toString();
		System.out.println("Subject : " + subject);
		System.out.println("Received Date : " + receivedDate.toString());
		System.out.println("Content : " + content);
		getContent(message);
	}
	public void getContent(Message msg)
	{
		try
		{
			String contentType = msg.getContentType();
			System.out.println("Content Type : " + contentType);
			Multipart mp = (Multipart) msg.getContent();
			int count = mp.getCount();
			for (int i = 0; i < count; i++)
			{
				dumpPart(mp.getBodyPart(i));
			}
		}
		catch (Exception ex)
		{
			System.out.println("Exception arise at get Content");
			ex.printStackTrace();
		}
	}
	public void dumpPart(Part p) throws Exception
	{
		// Dump input stream ..
		InputStream is = p.getInputStream();
		// If "is" is not already buffered, wrap a BufferedInputStream
		// around it.
		if (!(is instanceof BufferedInputStream))
		{
			is = new BufferedInputStream(is);
		}
		int c;
		System.out.println("Message : ");
		while ((c = is.read()) != -1)
		{
			System.out.write(c);
		}
	}
	public String getSubject() {
		return subject;

	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Address[] getTarget() {
		return target;
	}
	public void setTarget(Address[] target) {
		this.target = target;
	}
	public Address[] getSource() {
		return source;
	}
	public void setSource(Address[] source) {
		this.source = source;
	}
}
