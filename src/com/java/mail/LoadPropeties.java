package com.java.mail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.swing.JOptionPane;

public class LoadPropeties {

	public String SMTPserver;
	public String Port;
	public String Mailuser;
	public String Passwduser;
	public String Subjectmessage;
	public String Bodymessage;
	public String Mailuserimap ;
	public String Passwduserimap ;
	public String Excelpath;
	public String Subjectmessageconfirm;
	public String Bodymessageconfirm;
	public String MailtosendNITnotfound;
	public String SubjectNITnotfound;
	public String BodymessageNITnotfound;
	public String SubjectNITnotfoundsource;
	public String BodymessageNITnotfoundsource;
	public String timerefresh;

	public LoadPropeties() throws InvalidPropertiesFormatException, IOException{
		
		Properties propSend = new Properties();
		
	//String dirpath = System.getProperty("user.dir");
	//InputStream mailSend = new FileInputStream(dirpath+"/config.xml");
	InputStream mailSend = new FileInputStream("config.xml");
	//arriba modificando el classpath del meta-inf hace que sirva 
		
		propSend.loadFromXML(mailSend);
		SMTPserver = propSend.getProperty("SMTPserver");
		Port = propSend.getProperty("Port");
		Mailuser = propSend.getProperty("Mailuser");
		Passwduser = propSend.getProperty("Passwduser");
		Subjectmessage = propSend.getProperty("Subjectmessage");
		Bodymessage = propSend.getProperty("Bodymessage");
		Mailuserimap = propSend.getProperty("Mailuserimap");
		Passwduserimap = propSend.getProperty("Passwduserimap");
		Excelpath = propSend.getProperty("Excelpath");
		Subjectmessageconfirm = propSend.getProperty("Subjectmessageconfirm");
		Bodymessageconfirm = propSend.getProperty("Bodymessageconfirm");
		MailtosendNITnotfound = propSend.getProperty("MailtosendNITnotfound");
		SubjectNITnotfound = propSend.getProperty("SubjectNITnotfound");
		BodymessageNITnotfound = propSend.getProperty("BodymessageNITnotfound");
		SubjectNITnotfoundsource = propSend.getProperty("SubjectNITnotfoundsource");
		BodymessageNITnotfoundsource = propSend.getProperty("BodymessageNITnotfoundsource");
		timerefresh = propSend.getProperty("Timerefresh");
	}
	public String getSMTPserver() {
		return SMTPserver;
	}
	public void setSMTPserver(String sMTPserver) {
		SMTPserver = sMTPserver;
	}
	public String getPort() {
		return Port;
	}
	public void setPort(String port) {
		Port = port;
	}
	public String getMailuser() {
		return Mailuser;
	}
	public void setMailuser(String mailuser) {
		Mailuser = mailuser;
	}
	public String getPasswduser() {
		return Passwduser;
	}
	public void setPasswduser(String passwduser) {
		Passwduser = passwduser;
	}
	public String getSubjectmessage() {
		return Subjectmessage;
	}
	public void setSubjectmessage(String subjectmessage) {
		Subjectmessage = subjectmessage;
	}
	public String getBodymessage() {
		return Bodymessage;
	}
	public void setBodymessage(String bodymessage) {
		Bodymessage = bodymessage;
	}

	public String getMailuserimap() {
		return Mailuserimap;
	}

	public void setMailuserimap(String mailuserimap) {
		Mailuserimap = mailuserimap;
	}

	public String getPasswduserimap() {
		return Passwduserimap;
	}

	public void setPasswduserimap(String passwduserimap) {
		Passwduserimap = passwduserimap;
	}

	public String getExcelpath() {
		return Excelpath;
	}

	public void setExcelpath(String excelpath) {
		Excelpath = excelpath;
	}

	public String getSubjectmessageconfirm() {
		return Subjectmessageconfirm;
	}

	public void setSubjectmessageconfirm(String subjectmessageconfirm) {
		Subjectmessageconfirm = subjectmessageconfirm;
	}

	public String getBodymessageconfirm() {
		return Bodymessageconfirm;
	}

	public void setBodymessageconfirm(String bodymessageconfirm) {
		Bodymessageconfirm = bodymessageconfirm;
	}

	public String getMailtosendNITnotfound() {
		return MailtosendNITnotfound;
	}
	public void setMailtosendNITnotfound(String mailtosendNITnotfound) {
		MailtosendNITnotfound = mailtosendNITnotfound;
	}
	public String getSubjectNITnotfound() {
		return SubjectNITnotfound;
	}
	public void setSubjectNITnotfound(String subjectNITnotfound) {
		SubjectNITnotfound = subjectNITnotfound;
	}
	public String getBodymessageNITnotfound() {
		return BodymessageNITnotfound;
	}
	public void setBodymessageNITnotfound(String bodymessageNITnotfound) {
		BodymessageNITnotfound = bodymessageNITnotfound;
	}
	public String getSubjectNITnotfoundsource() {
		return SubjectNITnotfoundsource;
	}
	public void setSubjectNITnotfoundsource(String subjectNITnotfoundsource) {
		SubjectNITnotfoundsource = subjectNITnotfoundsource;
	}
	public String getBodymessageNITnotfoundsource() {
		return BodymessageNITnotfoundsource;
	}
	public void setBodymessageNITnotfoundsource(String bodymessageNITnotfoundsource) {
		BodymessageNITnotfoundsource = bodymessageNITnotfoundsource;
	}

	public String getTimerefresh() {
		return timerefresh;
	}

	public void setTimerefresh(String timerefresh) {
		this.timerefresh = timerefresh;
	}


}
